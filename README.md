# some middleware set for docker

* using rails, flask, etc, it's good to connect these middleware.
  - mysql
  - postgresql
  - mongodb
  - redis
  - elasticsearch
  - memchached

* auth info
  - mysql: root/root
  - postgresql: db/db
  - others: no auth

* opened port
  everything default :)
  - mysql: 3306
  - postgresql: 5432
  - mongodb: 27017
  - redis: 6379
  - elasticsearch: 9200/9300
  - memcached: 11211

* using
```
docker-compose up
or
docker-compose up -d
```

  - docker-toolbox
    IP: 192.168.99.100

  - docker-ce
   IP: 127.0.0.1